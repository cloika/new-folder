#include "stdafx.h"
#include <iostream>
#include "Slist.h"
using namespace std;

Single_node::Single_node(int x, Single_node *j)
{
	val = x;
	next = j;
}

Single_list::Single_list()
{
	tail = new Single_node;
	head = tail;
}
Single_list::Single_list(int a)
{
	tail = new Single_node;
	head = new Single_node(a, tail);
}

bool Single_list::empty()
{
	return head == tail;
}
void Single_list::push_front(int a)
{
	head = new Single_node(a, head);
}
void Single_list::pop_front()
{
	Single_node *node = head->next;
	delete head;
	head = node;
}
void Single_list::clear()
{
	while (head != tail)
	{
		Single_node *node = head->next;
		delete head;
		head = node;
	}

}
int & Single_list::front()
{
	return head->val;
}
int Single_list::size()
{
	Single_node *h = head;
	int k = 0;
	while (h != tail)
	{
		h = h->next;
		k++;
	}
	return k;
}
void Single_list::print()
{
	for (Single_node* p = head; p != tail; p = p->next)
		cout << p->val << " ";
	cout << "\n";
}
Single_list::~Single_list()
{

	while (head != tail)
	{
		Single_node *node = head->next;
		delete head;
		head = node;
	}
	delete tail;

}