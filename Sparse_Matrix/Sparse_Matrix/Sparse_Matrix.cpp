// Sparse_Matrix.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
using namespace std;
struct Cell {
	int row;
	int col;
	Cell() {};
	Cell(int, int, Cell *, int, Cell *);
	Cell *right;
	Cell *down;
	int data;
};
Cell::Cell(int val, int rows, Cell *r, int cols, Cell *d)
{
	data = val;
	row = rows;
	right = r;
	col = cols;
	down = d;
	}
class Sparse_Matrix {
private:
	int n_rows;
	int n_cols;
	int def_value;
	Cell *head;
public:
	Sparse_Matrix(int, int, int);
	~Sparse_Matrix();
	int r_access(int, int) const;
	int& w_access(int, int);
	void print_r();//[(0,2)7]
	void print_c();
};
Sparse_Matrix::Sparse_Matrix(int rows, int cols, int def)
{
	n_cols = cols;
	n_rows = rows;
	def_value = def;
	head = new Cell(-42, -1,0, -1, 0);
	Cell *h = head;
	for (int i = 0; i < cols; i++)
	{
		Cell *help = new Cell(-42, -1, 0, i, 0);
		h->right = help;
		h = help;
	}
	h = head;
	for (int i = 0; i < rows; i++)
	{
		Cell *help = new Cell(-42, i, 0, -1, 0);
		h->down= help;
		h = help;
	}

}
int Sparse_Matrix::r_access(int r, int c) const 
{
	Cell *help = head;
	while (help->row<r)
	{
		help = help->down;
	}
	if (help->row == r)
	{
		while (help->right && help->col < c)
		{
			help = help->right;
		}
		if (help->col == c)
			return help->data;
		else
			return def_value;
	}
	else
		return def_value;
}
int& Sparse_Matrix::w_access(int r, int c)
{
	Cell *help = head;
	while (help->row < r)
		help = help->down;
	while (help->right && help->right->col < c)
		help = help->right;
	if (help->col == c)
		return help->data;
	else
	{
		Cell *t = new Cell(def_value, r, help->right, c, 0);
		help->right = t;
		help = head;
		while (help->col < c)
			help = help->right;
		while (help->down && help->down->row < r)
			help->down;
		t->down = help->down;
		help->down = t;
		return t->data;
	}
	

}
void Sparse_Matrix::print_c()
{
	Cell *help = head->right;
	for (int i = 0; i < n_rows; i++)
	{
		Cell *help2 = help->down;
		for (int j = 0; j < n_cols; j++)
		{
			cout << "[(" << j << "," << i << ")" << help2->data << "] ";
			help2 = help2->down;
		}
		help = help->right;
	}

}
void Sparse_Matrix::print_r()
{
	Cell *help = head->down;
	for (int i = 0; i < n_rows; i++)
	{
		if (help->right) {
			Cell *help2 = help->right;
			for (int j = 0; help2 && j < n_cols; j++)
			{
				cout << "[(" << help2->row << "," << help2->col << ")" << help2->data << "] ";
				help2 = help2->right;
			}
			cout << endl;
		}
		help = help->down;
	}
}
Sparse_Matrix::~Sparse_Matrix()
{

}
int main()
{
		Sparse_Matrix M(3, 5, -1);
		M.w_access(2, 3) = -7;
		cout << M.r_access(2, 3) << endl;
		M.print_r();
		cout << M.r_access(0, 1) << endl;
		M.w_access(2, 4) = -5;
		M.print_r();
		M.w_access(2, 1) = -4;
		M.w_access(2, 0) = -4;
		M.print_r();
		M.w_access(0, 1) = -8;
		M.w_access(1, 3) = -9;
		M.print_r();
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 5; j++)
				M.w_access(i, j) = i + j;
		cout << "sdf";
		M.print_r();

    return 0;
}

