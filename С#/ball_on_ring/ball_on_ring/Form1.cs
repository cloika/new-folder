﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ball_on_ring
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int x = 50, y = 50, r = 50;
        bool fix = false;
        private void Form1_Click(object sender, EventArgs e)
        {
            fix = !fix;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!fix)
            {
                x+= e.X-x;
                y += e.Y-y;
            }
            Refresh();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics ball = e.Graphics;
            ball.FillEllipse(Brushes.Red, x, y, r, r);

        }
    }
}
