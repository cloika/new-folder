﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test1
{
    public partial class Form1 : Form
    {
        float x=10, y=10;
        int r1 = 30, r2 = 30;
        float vx = 5, vy = 5;
        float dx=0, dy=0;
        bool isMoved = false;

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if((x+r1-e.X)* (x + r1 - e.X)+ (y + r1 - e.Y)* (y + r1 - e.Y)<=r1*r1)
            {
                isMoved = true;
                dx = e.X - x;
                dy = e.Y - y;
            }
            else
            {
                if (this.BackColor == Color.Red)
                    this.BackColor = Color.Empty;
                else
                    this.BackColor = Color.Red;
            }

        }
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMoved)
            {
                x = e.X - dx;
                y = e.Y - dy;
                Refresh();
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (isMoved)
            {
                x = e.X - dx;
                y = e.Y - dy;
                Refresh();
            }
            isMoved = false;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W: { y -= 5; if (x < 0) x = 0; break; }
                case Keys.A: {x -= 5; if (y < 0) y = 0; break; }
                case Keys.D: { x += 5; if ((x + 2 * r1) > ClientRectangle.Width) x = ClientRectangle.Width - 2*r1; break; }
                case Keys.S: { y += 5; if ((y + 2 * r1) > ClientRectangle.Height) y = ClientRectangle.Height - 2 * r1; break; }
                case Keys.Down: {if(r1>5) r1 -= 1;break; }
                case Keys.Up: { if(r1<100)r1 += 1; break; }
            }
            Refresh();
        }
        Color clr = Color.FromName("s");
        Random rnd = new Random();
        private void timer1_Tick(object sender, EventArgs e)
        {
            x += vx; y += vy;
            if (x < 0)
            {
                x = 0;
                vx = -1 * vx * ((float)rnd.NextDouble() + (float)0.7);
            }
            if (y < 0)
            {
                y = 0;
                vy = -1* vy * ((float)rnd.NextDouble() + (float)0.8);
            }
            if ((x+ 2 * r1) >= ClientRectangle.Width)
            {
                x = ClientRectangle.Width -2*r1;
                vx = -1* vx * ((float)rnd.NextDouble() + (float)0.4);
            }
            if ((y + 2 * r1) >= ClientRectangle.Height)
            {
                y = ClientRectangle.Height - 2 * r1;
                vy = -1* vy * ((float)rnd.NextDouble() + (float)0.3);
            }
            
            
            Refresh();
        }

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.FillEllipse(Brushes.Orange, x, y, 2*r1, 2*r1);
        }
    }
}
