﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace almost_paint
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            bmp = new Bitmap(ClientSize.Width, ClientSize.Height);
            g = Graphics.FromImage(bmp);
            g.FillRectangle(new SolidBrush(BackColor), ClientRectangle);
        }

        bool isDraw = false;
        Point begin, end;
        //List<Point> points = new List<Point>();
        Bitmap bmp;
        Graphics g;

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            isDraw = true;
            begin = e.Location;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            //if(isDraw)
            //{
            //    Graphics g = CreateGraphics();
            //    g.DrawLine(Pens.Black, begin_x, begin_y, e.X, e.Y);
            //    begin_x = e.X;
            //    begin_y = e.Y;

            //}
            if (isDraw)
            {
                Rectangle rect = new Rectangle(Math.Min(begin.X, end.X)-1, Math.Min(begin.Y, end.Y)-1, Math.Abs(begin.X - end.X), Math.Abs(begin.Y - end.Y));
                end = e.Location;
                Invalidate(rect);
          
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            //if (isDraw)
            //{
            //    Graphics g = CreateGraphics();
            //    g.DrawLine(Pens.Black, begin_x, begin_y, e.X, e.Y);
            //    begin_x = e.X;
            //    begin_y = e.Y;
            //    isDraw = false;
            //}
            if (isDraw)
            {
                //points.Add(begin);
                //points.Add(end);
                g.DrawLine(Pens.Black,begin, end);
                end = e.Location;
                isDraw = false;

            }
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(bmp,e.ClipRectangle,e.ClipRectangle,GraphicsUnit.Pixel);
            if (isDraw)
            {
                e.Graphics.DrawLine(Pens.Black, begin, end);
            }
        }
    }
}
