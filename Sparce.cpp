#include <iostream>
using namespace std;

struct Cell{
    int col;
    Cell* right;
    int data;
    Cell(){}
    Cell(int);
    Cell(int,Cell *,int);
};
Cell::Cell(int c) {
    col = c;
}
Cell::Cell(int c, Cell* r, int d) {
    col = c;
    right = r;
    data = d;
}
class Sparce_Array{
private:
    Cell* head;
    int default_value;
public:
    Sparce_Array(int);
    ~Sparce_Array();
    int& operator[](int);
    void print();
};
Sparce_Array::Sparce_Array(int x) {
    head = new Cell(-1);
    head->right = 0;
    default_value = x;
}
Sparce_Array::~Sparce_Array(){
    while (head)//->right != 0)
    {
        Cell *temp = head->right;
        delete head;
        head = temp;
    }
    //delete head;
}
int& Sparce_Array::operator[](int c) {
    Cell *temp = head;
  //  temp = head;
    while (temp->right && c > temp->right->col) temp = temp->right;
  /*  if (temp->right == 0) {
        Cell *add = new Cell(c, 0, default_value);
        temp->right = add;
        return add->data;
    }
    else */if (temp->right && c == temp->right->col) return temp->right->data;
    else {
        Cell *add = new Cell(c, temp->right, default_value);
        temp->right = add;
        return add->data;
    }
}
void Sparce_Array::print(){
    Cell *temp = head->right;
    while (temp) {
        cout << '[' << temp ->col << ',' << temp->data << ']' << ", ";
        temp = temp -> right;
    }
    cout<< endl;
//    cout << '[' << temp ->col << ',' << temp->data << ']' << endl;
}
int main() {
    Sparce_Array A(-2);
    for (int i = 0; i < 20; i+=2) A[i] = i+1;
    A.print();
    A[20];
    A[1];
    A[1000000] = 1499;
    A.print();
    for (int i = 54; i < 80; i+=3)
      if(i%2) A[i];
      else A[i] = i*3;
    A.print();
    cout << A[18];
    return 0;
}
