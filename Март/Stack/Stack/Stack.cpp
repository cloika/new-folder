
#include "stdafx.h"
#include <iostream>
#include "Slist.h"
using namespace std;

class Stack {
private:
	int sz;
	Single_list* list;
public:
	Stack();
	~Stack();
	bool empty();
	void push(int);
	void pop();
	void clear();
	int& top();
	int size();
	void print();
};
Stack::Stack()
{
	list = new Single_list();
}
bool Stack::empty()
{
	return !list->size();
}
void Stack::push(int a)
{
	list->push_front(a);
}
void Stack::pop()
{
	list->pop_front();
}
int & Stack::top()
{
	return  list->front();
}
int Stack::size()
{
	return list->size();
}
void Stack::print()
{
	list->print();
}
Stack::~Stack()
{

}
void error_msg(bool check)
{
	if (!check)
		cout << "ERROR";
}

class Stack1
{
private:
	int head;
	int *ar;
	int s_a;
public:
	Stack1(int);
	~Stack1();
	int size();
	void push(int);
	int& top();
	void pop();
	bool empty();
};
Stack1::Stack1(int n)
{
	ar = new int[n];
	//head = 0;
	s_a = 0;
}
void Stack1::push(int a)
{
	ar[s_a++] = a;
	//	s_a++;
}
void Stack1::pop()
{
	s_a--;
}
int& Stack1::top()
{
	return ar[s_a-1];
}
bool Stack1::empty()
{
	return !s_a;
}
Stack1::~Stack1()
{
	delete[] ar;
}
int Stack1::size()
{
	return s_a;
}

int main()
{
	Stack S;
	S.push(8);
	S.push(7);
	S.push(4);
	error_msg(S.size() == 3);
	error_msg(S.top() == 4);
	S.pop();
	error_msg(S.top() == 7);
	S.pop();
	error_msg(S.top() == 8);
	S.pop();
	error_msg(S.empty());

	Stack1 S1(4);

	S1.push(8);
	S1.push(7);
	S1.push(4);

	error_msg(S1.size() == 3);
	error_msg(S1.top() == 4);
	S1.pop();
	error_msg(S1.top() == 7);
	S1.pop();
	error_msg(S1.top() == 8);
	S1.pop();
	error_msg(S1.empty());
	return 0;
}

