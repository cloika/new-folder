#pragma once
struct Single_node {
	int val;
	Single_node * next;
	Single_node() {}
	Single_node(int, Single_node *);
};
class Single_list {
private:
	Single_node * head;
	Single_node *tail;
public:
	Single_list();
	Single_list(int);
	~Single_list();
	bool empty();
	void push_front(int);
	void pop_front();
	void clear();
	int& front();
	int size();
	void print();
};