#include <iostream>

using namespace std;


struct Cell
{
    int col;
    Cell * right;
    int data;
    Cell(){}
    Cell(int);
    Cell(int,Cell *,int);
};
Cell::Cell(int c,Cell *r,int d)
{
    col=c;
    right=r;
    data=d;
}
Cell::Cell(int c)
{
    col=c;
}

class Sparse_Array
{
private:
    Cell * head;
    int default_value;
public:
    Sparse_Array(int);
    ~Sparse_Array();
    int& operator[](int);
    void print();
};

Sparse_Array::Sparse_Array(int a)
{
    default_value = a;
    head = new Cell(-1);
    head->right = 0;
}

Sparse_Array::~Sparse_Array()
{

    while (head) {
        Cell *k = head->right;
        delete head;
        head = k;
    }

}

int& Sparse_Array::operator[](int a)
{
    Cell *check = head;
    while (check->right && a > check->right->col) check = check->right;

    if (check->right && a == check->right->col)
    {
        return check->data;
    }
    else
    {
        Cell *add = new Cell(a,check->right,default_value);
        check->right = add;
        return add->data;
    }
}

void Sparse_Array::print()
{
    Cell *t = head;
      while (t->right != 0)
        {
            t = t->right;
            cout << "[" << t->col << "," << t->data << "] ";
        }
        cout<<endl;
}

int main()
{
    Sparse_Array l(-1);
    for (int i = 9; i>=4 ; i--)
        l[i] = i;
    l[3]+=l[0]+l[2]+5;
    l.print();
    return 0;
}
