#include <iostream>
using namespace std;

struct Single_node {
	int val;
	Single_node * next;
	Single_node() {}
	Single_node(int, Single_node *);
};
Single_node::Single_node(int x, Single_node *j)
{
	val = x;
	next = j;
}
class Queue {
private:
	int sz;
	Single_node * head;
	Single_node *tail;
public:
	Queue();
	~Queue();
	bool empty();
	void push(int);
	void pop();
	void clear();
	int& front();
	int& back();
	int size();
	void print();
};
Queue::Queue()
{
	sz = 0;
	tail = new Single_node;
	head = tail;
}
bool Queue::empty()
{
	return sz==0;
}
void Queue::push(int a)
{
	if (!sz)
	{
		head = tail = new Single_node(a, tail);
	}
	else
	{
		Single_node *help = new Single_node(a, NULL);
		tail->next = help;
		tail = help;
	}
	sz++;
}
void Queue::pop()
{
	Single_node *node = head->next;
	delete head;
	head = node;
	sz--;
}
void Queue::clear()
{
	while (head != tail)
	{
		Single_node *node = head->next;
		delete head;
		head = node;
	}
	sz = 0;
}
int& Queue::front()
{
	return head->val;
}
int& Queue::back()
{
	return tail->val;
}
int Queue::size()
{
	return sz;
}
void Queue::print()
{
	for (Single_node* p = head; p != tail; p = p->next)
		cout << p->val << " ";
	cout << "\n";
}
Queue::~Queue()
{

	while (sz)
	{
		Single_node *node = head->next;
		delete head;
		head = node;
	}
}
void error_msg(bool check)
{
	if (!check)
		cout << "ERROR";
}

class Queue1
{
	private:
		int head;
		int tail;
		int n;
		int *ar;
		int s_a;
	public:
		Queue1(int);
		~Queue1();
		int size();
		void push(int);
		int& front();
		int& back();
		void pop();
		bool empty();
};
Queue1::Queue1(int n1)
{
	int *arr = new int[n1];
	ar = arr;
	head = 0;
	tail = 0;
	s_a = 0;
	n = n1;
}
void Queue1::push(int a)
{
	ar[head%n] = a;
	head++;
	s_a++;
}
void Queue1::pop()
{
	tail++;
	s_a--;
}
int& Queue1::front()
{
	return ar[tail%n];
}

int & Queue1::back()
{
	return ar[tail];
}
bool Queue1::empty()
{
	return !s_a;
}
Queue1::~Queue1()
{
	delete ar;
}
int Queue1::size()
{
	return s_a;
}

int main()
{
	Queue Q;
	Q.push(8);
	Q.push(7);
	Q.push(6);
	Q.push(2);
	error_msg(Q.size() == 4);
	error_msg(Q.back() == 2);
	error_msg(Q.front() == 8);
	Q.pop();
	error_msg(Q.front() == 7);
	Q.pop();
	error_msg(Q.front() == 4);
	Q.pop();
	error_msg(Q.empty());

	Queue1 Q1(4);
	Q1.push(8);
	Q1.push(7);
	Q1.push(6);
	Q1.push(2);
	error_msg(Q1.size() == 4);
	error_msg(Q1.back() == 2);
	error_msg(Q1.front() == 8);
	Q1.pop();
	error_msg(Q1.front() == 7);
	Q1.pop();
	error_msg(Q1.front() == 4);
	Q1.pop();
	error_msg(Q1.empty());

    return 0;
}
