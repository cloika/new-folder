#include <iostream>

using namespace std;
struct Double_node{
int val;
Double_node * next;
Double_node * prev;
Double_node(){}
Double_node(int,Double_node *,Double_node *);
};

Double_node::Double_node(int a,Double_node *p,Double_node *n)
{
    val = a;
    prev = p;
    next = n;
}

class Double_list{
private:
    Double_node *tail;
    int s_z;
public:
    Double_list();// +++
    Double_list(int);  //+++++
    ~Double_list(); //++
    bool empty(); // +
    void push_front(int); //++++
    void pop_front(); //+++
    void push_back(int); //+++
    void pop_back(); //++
	void clear(); //+++
    int& front(); //++++
    int& back(); //+++
	int size(); //+++
    void print(); //+++
};

Double_list::Double_list()
{
    tail= new Double_node;
    tail->next=tail;
    tail->prev=tail;

    s_z=0;
}
Double_list::Double_list(int a)
{
        tail = new Double_node;
        Double_node *k = new Double_node(a,tail,tail);
        tail->next=k;
        tail->prev=k;
        s_z=1;
}

bool Double_list::empty()
{
        return !s_z;
}
void Double_list::push_front(int a)
{
    Double_node *l=new Double_node(a,tail,tail->next);
    tail->next->prev=l;
    tail->next=l;

    s_z++;
}
void Double_list::push_back(int a)
{
    Double_node *l= new Double_node(a,tail->prev,tail);
    tail->prev->next=l;
    tail->prev=l;
    s_z++;
}
void Double_list::pop_front()
{
        Double_node *node = tail->next->next;
        delete tail->next;
        tail->next = node;
        node->prev=tail;
        s_z--;
}
void Double_list::pop_back()
{
        Double_node *node = tail->prev->prev;
        delete tail->prev;
        tail->prev = node;
        node->next=tail;
        s_z--;
}
void Double_list::clear()
{
    Double_node *h=tail->next;
    while(h!=tail)
    {
        h=h->next;
        delete tail->next;
        tail->next =h;
    }
    s_z=0;
    tail->prev=tail;
}
int& Double_list::front()
{
    return tail->next->val;
}
int& Double_list::back()
{
    return tail->prev->val;
}
int Double_list::size()
{
	return s_z;
}
void Double_list::print()
{
	Double_node *p=tail->next;
    while(p != tail)
    {
        cout<<p->val<<" ";
        p=p->next;
    }
    cout<<"\n";
}
Double_list::~Double_list()
{
	while(tail->next != tail )
    {
        Double_node *node = tail->next->next;
        delete tail->next;
        tail->next = node;
    }
    delete tail;
}
//*/


int main()
{
	Double_list test;
	if(test.empty())
        cout<<"+"<<endl;
    for(int i=4;i<=10;i++)
    {
        test.push_back(i);
    }
    test.print();
    test.pop_back();
    test.print();
    cout<<test.size();
            //cout<<test.tail->prev<<" "<<test.tail<<" "<<test.tail->next;
    if(!test.empty())
        cout<<"-"<<endl;
    	return 0;
}
