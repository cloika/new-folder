#include <iostream>
using namespace std;


int main()
{
	int n;
	cin >> n;
	int A[100];
	for (int i = 0; i < n; i++)
		cin >> A[i];

	int k = 0, a = 0;
	while (k < 2) {
		if (A[a] % 2 != 0)
			k++;
		a++;
	}
	a--;

	int b = n - 1;
	while (A[b] >= 0)
		b--;

	for (int i = a; i < b; i++) {
		for (int j = a; j < b; j++)
			if (A[j] < A[j + 1])
				swap(A[j], A[j + 1]);
	}
	for (int i = 0; i < n; i++)
		cout << A[i] << ' ';

	return 0;
}
