#include <iostream>
using namespace std;
int pal(int r, char *s, int *prev, int *next) {
    next[r] = 1;
    for(int j=0;s[j];j++)
        cout<<next[j]<<" ";
        cout<<endl;
    for (int i = r-1; i >= 0; i-- ) {

        swap(prev,next);
        next[i] = 1;
        for (int j = i+1; j <= r; j++)
            if (s[i] == s[j])
                next[j] = prev[j - 1] + 2;
            else
                next[j] = max(prev[j], next[j - 1]);
        for(int j=0;s[j];j++)
        cout<<next[j]<<" ";
        cout<<endl;
    }
    return next[r];
}

int main() {
    char s[50];
    cin>>s;
    int n;
    for (n = 0; s[n]; n++);
    int *A = new int [n];
    int *B = new int [n];
    for(int i=0;i<n;i++)
        A[i]=0,B[i]=0;
    cout << pal(n-1, s, A, B);
    delete [] A;
    delete [] B;
}
