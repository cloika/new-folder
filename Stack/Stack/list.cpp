#include <iostream>
using namespace std;

struct Single_node{
int val;
Single_node * next;
Single_node() {}
Single_node(int ,Single_node *);
};

Single_node::Single_node(int x,Single_node *j)
{
    val=x;
    next=j;
}


class Single_list{
private:
    Single_node *head;
    Single_node *tail;
public:
    Single_list();
    Single_list(int);
    ~Single_list();
    bool empty();
    void push_front(int);
    void pop_front();
    void clear();
    int& front();
    int size();
    void print();
};

Single_list::Single_list()
{
    tail=new Single_node;
    head=tail;
}
Single_list::Single_list(int a)
{
            tail = new Single_node;
			head=new Single_node(a,tail);
}

bool Single_list::empty()
{
    return head==tail;
}
void Single_list::push_front(int a)
{
    head=new Single_node(a,head);
}
void Single_list::pop_front()
{
        Single_node *node = head->next;
        delete head;
        head = node;
    }
void Single_list::clear()
{
    while(head != tail)
    {
        Single_node *node = head->next;
        delete head;
        head = node;
    }

}
int& Single_list::front()
{
    return head->val;
}
int Single_list::size()
{
    Single_node *h=head;
    int k=0;
    while(h != tail)
    {
        h=h->next;
        k++;
    }
    return k;
}
void Single_list::print()
{
    for (Single_node* p=head; p!=tail; p=p->next)
        cout<<p->val<<" ";
    cout<<"\n";
}
Single_list::~Single_list()
{

   while(head != tail )
    {
        Single_node *node = head->next;
        delete head;
        head = node;
    }
    delete tail;

}


int main(){
Single_list test(5);
for(int i=0;i<5;i++)
{
    test.push_front(i);
    test.print();
}
test.front()=9;

if(test.empty())
{
    cout<<"+";
}
else
{
    cout<<"-";
}
cout<<test.size();
cout<<endl;
test.print();
test.clear();
if(test.empty())
{
    cout<<"+";
}
else
{
    cout<<"-";
}
return 0;
}
