
#include "stdafx.h"
#include <iostream>
#include <list.h>
using namespace std;

struct Single_node {
	int val;
	Single_node * next;
	Single_node() {}
	Single_node(int, Single_node *);
};
Single_node::Single_node(int x, Single_node *j)
{
	val = x;
	next = j;
}
class Stack {
private:
	int sz;
	Single_node * head;
	Single_node *tail;
public:
	Stack();
	~Stack();
	bool empty();
	void push(int);
	void pop();
	void clear();
	int& top();
	int size();
	void print();
};
Stack::Stack()
{
	sz = 0;
	tail = new Single_node;
	head = tail;
}
bool Stack::empty()
{
	return sz == 0;
}
void Stack::push(int a)
{
	head = new Single_node(a, head);
	sz++;
}
void Stack::pop()
{
	Single_node *node = head->next;
	delete head;
	head = node;
	sz--;
}
void Stack::clear()
{
	while (head != tail)
	{
		Single_node *node = head->next;
		delete head;
		head = node;
	}
	sz = 0;
}
int& Stack::top()
{
	return head->val;
}
int Stack::size()
{
	return sz;
}
void Stack::print()
{
	for (Single_node* p = head; p != tail; p = p->next)
		cout << p->val << " ";
	cout << "\n";
}
Stack::~Stack()
{

	while (sz)
	{
		Single_node *node = head->next;
		delete head;
		head = node;
	}
	delete tail;

}
void error_msg(bool check)
{
	if (!check)
		cout << "ERROR";
}

class Stack1
{
private:
	int head;
	int *ar;
	int s_a;
public:
	Stack1(int);
	~Stack1();
	int size();
	void push(int);
	int& top();
	void pop();
	bool empty();
};
Stack1::Stack1(int n)
{
	ar = new int[n];
	//head = 0;
	s_a = 0;
}
void Stack1::push(int a)
{
		ar[s_a++] = a;
//	s_a++;
}
void Stack1::pop()
{
	s_a--;
}
int& Stack1::top()
{
	return ar[head];
}
bool Stack1::empty()
{
	return !s_a;
}
Stack1::~Stack1()
{
	delete[] ar;
}
int Stack1::size()
{
	return s_a;
}

int main()
{
	Stack S;
	S.push(8);
	S.push(7);
	S.push(4);
	error_msg(S.size() == 3);
	error_msg(S.top() == 4);
	S.pop();
	error_msg(S.top() == 7);
	S.pop();
	error_msg(S.top() == 8);
	S.pop();
	error_msg(S.empty());


	Stack1 S1(4);

	S1.push(8);
	S1.push(7);
	S1.push(4);

	error_msg(S1.size() == 3);
	error_msg(S1.top() == 4);
	S1.pop();
	error_msg(S1.top() == 7);
	S1.pop();
	error_msg(S1.top() == 8);
	S1.pop();
	error_msg(S1.empty());
	/*cout << "Stack";*/
	//int x;
	//cin >> x;
	return 0;
}

