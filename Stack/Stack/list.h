bool empty();
void push_front(int);
void pop_front();
void clear();
int& front();
int size();
void print();